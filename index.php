<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 30/11/2018
 * Time: 15:06
 */
require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \wishlist\controle\ListControlleur as controleList;
use \wishlist\controle\ItemControlleur as controleItem;
use \wishlist\controle\UserControleur;
use \wishlist\vue\VueIndex as index;
use \wishlist\vue\VueCompte;

$db = new DB();
$conf=parse_ini_file('src/conf/conf.ini');
$db->addConnection( $conf );

$db->setAsGlobal();
$db->bootEloquent();


$app = new \Slim\Slim();


$app->get('/', function(){
  $i = new index();
  $i->render();
})->name('index');

//creation d'une liste
$app->get('/liste/create', function() {
    $ctrl=new controleList();
    $ctrl->newListe();

})->name('new_liste');

$app->post('/liste/create', function() {
    $ctrl=new controleList();
    $ctrl->saveListe();
});

//affichage des listes de souhaits
$app->get('/liste/souhait', function() {
    $ctrl=new controleList();
    $ctrl->getListeList();
});

//affichage des listes de l'utilisateur
$app->get('/liste/user', function() {
    $ctrl=new controleList();
    $ctrl->getListeUser();
})->name("Utilisateur");

//affichage des listes et de leurs items
$app->get('/liste/all', function (){
    $ctrl=new controleList();
    $ctrl->getAll();
});

//affichages des items d'une liste
$app->get('/liste/:idliste', function($idliste) {
    $ctrl=new controleList();
    $ctrl->getUneListe($idliste);
})->name('Message');

$app->post('/liste/:idliste', function($idliste) {
    $ctrl=new controleList();
    $ctrl->setMsg($idliste);
});

//partage de la liste
$app->get('/liste/partage/:token', function($token_partage){
    $ctrl=new controleList();
    $ctrl->shareList($token_partage);
});

$app->post('/liste/partage/:token', function($token_partage){
    $ctrl=new controleList();
    $ctrl->setMsg();
});


$app->post('/liste/edit/:te', function($te){
    $ctrl = new controleList();
    $ctrl->saveEdit();
})->name('modification');

$app->get('/liste/edit/delete/:te',function($te){
    $ctrl = new controleList();
    $ctrl->suppList($te);
})->name('suppression');

$app->post('/liste/edit/delete/:te', function($te){
    $ctrl = new controleList();
    $ctrl->saveSupp();
});

//creation d'un item
$app->get('/item/create', function() {
    $ctrl=new controleItem();
    $ctrl->newItem();
})->name('new_item');

$app->post('/item/create', function() {
    $ctrl=new controleItem();
    $ctrl->saveItem();
});

//affichage de l'item correspondant
$app->get('/item/:id', function($id){
    $ctrl=new controleItem();
    $ctrl->getItem($id);
});
//affichage du formulaire de modification d'item
$app->get('/item/modifier/:id', function($id){
    $ctrl=new controleItem();
    $ctrl->getmodifierItem($id);
})->name("ModifierItem");

//route de modification d'item
$app->post('/item/modifier/:id', function($id){
    $ctrl=new controleItem();
    $ctrl->modifierItem($id);
});

//route de suppression d'item
$app->post('/item/suppression/:id', function($id){
    $ctrl=new controleItem();
    $ctrl->supprimerItem($id);
});

//reservation d'un item
$app->get('/item/reserver/:id',function ($id){
    $ctrl=new controleItem();
    $ctrl->bookItem($id);
})->name('reservation');

$app->post('/item/reserver/:id',function ($id){
    $ctrl=new controleItem();
    $ctrl->saveBooking();
});

/**
*
* Routes de connexion
*
**/
$app->get('/Inscription',function(){
  $vue=new VueCompte();
  echo $vue->render(2);
})->name("Inscription");

$app->get('/Connexion',function(){
  $vue=new VueCompte();
  echo $vue->render(1);
})->name("Connexion");


$app->post('/Connexion',function(){
  $c = new UserControleur();
  $c->connexion();
});


$app->post('/Inscription',function(){
  $c = new UserControleur();
  $c->creation();
});

$app->run();
