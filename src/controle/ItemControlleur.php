<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 05/12/2018
 * Time: 11:50
 */

namespace wishlist\controle;

use \wishlist\modele\Item ;
use \wishlist\modele\Liste ;
use \wishlist\vue\VueParticipant as vue;

class ItemControlleur
{
  private $url;

  function __construct(){
    $this->url = $this->getUrl();
  }

  private function getUrl(){
    $app =\Slim\Slim::getInstance() ;
    return $app->request->getRootUri();
  }
    public function getItem($id){
        $item = Item::find($id);
        if(isset($item)){
          $vue=new vue($item->toArray());
          $vue->render(3);
        }
        else{
          $vue=new vue(array());
          $vue->render(-1);
        }
    }

    public function getmodifierItem($id){
        $item = Item::find($id);
        $v=new vue($item->toArray());
        $v->render(2);
    }

    public function supprimerItem($id){
      $item=Item::find($id);
      if($item->img!=null)
        unlink("./images/imagesItem/$item->img");
      $item->delete();
      header("Location: $this->url");
      exit;
    }
    public function modifierItem($id){
      $item = Item::find($id);

      $nom=filter_var($_POST["nom"],FILTER_SANITIZE_STRING);
      $item->nom=$nom;

      $descr=filter_var($_POST["descr"],FILTER_SANITIZE_STRING);
      $item->descr=$descr;

      if(!$_FILES['Image']['tmp_name']==""){
          //suppression de l'image de base
          if($item->img!=null)
            unlink("$this->url/images/imagesItem/$item->img");
          $type = preg_split("[/]",$_FILES['Image']['type']);
          $nomImage=$_FILES['Image']['size'].$item['nom'].".".$type[1];
          $nomImage=str_replace(" ","", $nomImage);
          $destination= './images/imagesItem/'.$nomImage;
          move_uploaded_file( $_FILES['Image']['tmp_name'],$destination);
          $item->img=$nomImage;
      }

      $url=filter_var($_POST["url"],FILTER_SANITIZE_STRING);
      $item->url=$url;

      $tarif=filter_var($_POST["tarif"],FILTER_SANITIZE_STRING);
      $item->tarif=$tarif;

      $item->save();
      header("Location: $this->url/item/$item->id");
      exit();

    }
    public function newItem(){
        $vue=new vue([]);
        $vue->render(5);
    }

    public function saveItem(){
        $item=new Item();

        $liste_id=filter_var($_POST["liste_id"],FILTER_SANITIZE_STRING);
        $item->liste_id=$liste_id;

        $nom=filter_var($_POST["nom"],FILTER_SANITIZE_STRING);
        $item->nom=$nom;

        $descr=filter_var($_POST["descr"],FILTER_SANITIZE_STRING);
        $item->descr=$descr;

        if(!$_FILES['Image']['tmp_name']==""){
            $type = preg_split("[/]",$_FILES['Image']['type']);
            $nomImage=$_FILES['Image']['size'].$item['nom'].".".$type[1];
            $nomImage=str_replace(" ","", $nomImage);
            $destination= './images/imagesItem/'.$nomImage;
		        move_uploaded_file( $_FILES['Image']['tmp_name'],$destination);
            $item->img=$nomImage;
  	    }

        $url=filter_var($_POST["url"],FILTER_SANITIZE_STRING);
        $item->url=$url;

        $tarif=filter_var($_POST["tarif"],FILTER_SANITIZE_STRING);
        $item->tarif=$tarif;

        $item->save();
        $app=\Slim\Slim::getInstance();
        $redirection = $app->request->getRootUri()."//liste/".$item->liste_id;
        header("Location: $redirection");
        exit();

    }

    public function bookItem($id){
        $item=Item::where("id","=",$id)->first();
        $vue=new vue([$item->toArray()]);
        $vue->render(8);
    }

    public function saveBooking(){
        $id=$_POST["id"];
        $item=Item::where("id","=",$id)->first();
        $participant=filter_var($_POST["participant"],FILTER_SANITIZE_STRING);
        if($item->participant==""){
          $item->participant=$participant;
          $message=filter_var($_POST["message"],FILTER_SANITIZE_STRING);
          $item->message=$message;
          $item->save();
        }
        $this->getItem($id);
    }

    public function setMsg($id){
        $id=$_POST["liste_id"];
        $li=Liste::where("no","=",$id)->first();

        $message=filter_var($_POST['message'], FILTER_SANITIZE_STRING);
        $li->message=$message;

        $li->save();
        $this->getListeItem($id);
    }

}
