<?php

namespace wishlist\controle;

use \wishlist\modele\User;
use \wishlist\vue\VueCompte;
use \wishlist\vue\VueIndex;
use \wishlist\vue\VueParticipant;


class UserControleur {

	public function __construc(){}

	public function connexion(){
		if(isset($_POST['user'])&&isset($_POST['mdp'])){
			$user=User::find($_POST['user']);
			if(isset($user)){
				if(password_verify($_POST['mdp'],$user->mdp)){
					session_start();
					$_SESSION['user'] = $_POST['user'];
					$app=\Slim\Slim::getInstance();
					$url=$app->urlFor("Utilisateur");
					header("Location: $url");
					exit;
				}else{
					$vue = new VueCompte("","red");
					$vue->render(1,"","Mot de passe invalide");
				}
			}else{
				$vue = new VueCompte("red");
				$vue->render(1,"Identifiant invalide");
			}
		}
	}

	public function creation(){
		if(isset($_POST['user'])&&isset($_POST['mdp'])&&isset($_POST['C_mdp'])){
			$user=User::find($_POST['user']);
			if($user==null){
				if($_POST['mdp']==$_POST['C_mdp']){
					$user=new User();
					$user->user=filter_var($_POST['user'],FILTER_SANITIZE_STRING);
					$user->mdp=password_hash(filter_var($_POST['mdp'],FILTER_SANITIZE_STRING),PASSWORD_DEFAULT);
					$user->save();
					$vue = new VueIndex();
					$vue->render();
				}else{
					$vue = new VueCompte();
					$vue->render(2,"Les mots de passe ne sont pas identiques !");
				}
			}else{
				$vue = new VueCompte("red");
				$vue->render(2,"Identifiant déjà existant");
			}
		}



	}
}
