<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 11/12/2018
 * Time: 13:00
 */

namespace wishlist\controle;

use \wishlist\modele\Liste;
use \wishlist\vue\VueParticipant as vue;
use \wishlist\vue\VueCompte;

class ListControlleur
{

    public function getListeList(){
      if(!isset($_SESSION))
      session_start();
      if(isset($_SESSION['user'])){
        $listl=Liste::where('public','=',1)->orWhere('user','=', $_SESSION['user'])->get();
      }
      else
        $listl=Liste::where('public','=',1)->get();
        $vue=new vue($listl->toArray());
        $vue->render(1);
    }

    public function getListeUser(){
      if(!isset($_SESSION))
        session_start();
      if(isset($_SESSION['user'])){
        $liste=Liste::where('user', "=", $_SESSION['user'])->get();
        $vue=new vue($liste->toArray());
        $vue->render(14);
      }
      else{
        $vue=new VueCompte();
        $vue->render(1);
      }
    }

    public function pageListeCree($id){
      $listl=Liste::find($id);
      $vue=new vue($listl->toArray());
      $vue->render(13);
    }

    public function getUneListe($id){
      $liste=Liste::find($id);
      if(isset($liste)){
        $vue=new vue($liste->toArray());
        $vue->render(6);
      }
      else{
        $vue=new vue(array());
        $vue->render(-1);
      }
    }

    public function getAll(){
      if(!isset($_SESSION))
      session_start();
      if(isset($_SESSION['user'])){
        $listl=Liste::where('public','=',1)->orWhere('user','=', $_SESSION['user'])->get();
      }
      else
        $listl=Liste::where('public','=',1)->get();
        $vue=new vue($listl->toArray());
        $vue->render(7);
    }

    public function newListe(){
        $vue=new vue([]);
        $vue->render(4);
    }

    public function saveListe(){

        $li=new Liste();

        //creation du token de partage
        $tok=random_bytes(4);
        $tok=bin2hex($tok);
        $tok="P".$tok;

        //creation du token d'edition
        $tokE=random_bytes(4);
        $tokE=bin2hex($tokE);
        $tokE="E".$tokE;

        $titre=$_POST["titre"];
        $titre=filter_var($titre,FILTER_SANITIZE_STRING);
        $li->titre=$titre;

        $user=filter_var($_POST["user"],FILTER_SANITIZE_STRING);
        $li->user=$user;

        $d=filter_var($_POST["description"],FILTER_SANITIZE_STRING);
        $li->description=$d;

        $exp=filter_var($_POST["expiration"],FILTER_SANITIZE_STRING);
        $li->expiration=$exp;

        $token=filter_var($tok,FILTER_SANITIZE_STRING);
        $li->token=$token;

        $tokenEdit=filter_var($tokE,FILTER_SANITIZE_STRING);
        $li->tokenEdit=$tokenEdit;

        if(isset($_POST['check'])){
          $li->public=1;
        }

        $li->save();
        $this->pageListeCree($li->no);
    }

    public function shareList($token_p){
        if($token_p[0] == 'E'){
            $liste = Liste::where("tokenEdit", "=", $token_p)->get();
            $vue = new vue($liste);
            $vue->render(9);
        }
        else {
            $liste = Liste::where("token", "=", $token_p)->first();
            $vue = new vue([$liste->toArray()][0]);
            $vue->render(6);
        }
    }

    public function editList($te){
        $list = Liste::where("tokenEdit","=",$te)->first();
        $vue = new vue([$list->toArray()]);
        $vue->render(9);
    }

    public function suppList($te){
        $list = Liste::where("tokenEdit","=",$te)->first();
        $vue = new vue([$list->toArray()]);
        $vue->render(10);
    }

    public function saveEdit(){
        $te=$_POST["tokenEdit"];
        $li = Liste::where("tokenEdit","=",$te)->first();

        if(!is_null($_POST["titre"]) && $_POST["titre"] != ""){
            $titre=$_POST["titre"];
            $titre=filter_var($titre,FILTER_SANITIZE_STRING);
            $li->titre=$titre;
        }

        if(!is_null($_POST["description"]) && $_POST["description"] != ""){
            $d=filter_var($_POST["description"],FILTER_SANITIZE_STRING);
            $li->description=$d;
        }

        if(!is_null($_POST["expiration"]) && $_POST["expiration"] != ""){
            $exp=filter_var($_POST["expiration"],FILTER_SANITIZE_STRING);
            $li->expiration=$exp;
        }

       if(isset($_POST["check"])) {
          $li->public = 1;
       }
       else if(!isset($_POST["check"])){
          $li->public = 0;
        }

        $li->save();
        $vue = new vue($li->toArray());
        $vue->render(6);
    }

    public function saveSupp(){
        $te = $_POST["tokenEdit"];
        $items = Item::where("liste_id",'=',$liste->no);
        foreach ($items as $item) {
          $controleur = new \wishlist\controle\ItemControlleur();
          $controleur->supprimerItem($item->id);
        }
        $liste->delete();
        $this->getListeList();
    }

    public function setMsg(){
        $id=$_POST["no"];
        $li=Liste::where("no","=",$id)->first();
        session_start();
        $message=filter_var($_POST['message'], FILTER_SANITIZE_STRING);
        if(isset($_SESSION['user'])){
          $message="$_SESSION[user] : ".$message;
        }
        $li->message=$_POST['messagePrecedent'].$message;
        $li->save();
        $this->getUneListe($id);
    }
}
