<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 04/12/2018
 * Time: 16:44
 */

namespace wishlist\vue;

class VueIndex
{


    public function css()
    {
      $html=<<<END
      header{
          background: url(images/header.jpg);
          text-align:center;
          padding-bottom:2%;
          background-size: contain;
        }
        .titre{
          display: inline-block;
          border:solid black 2px;
          border-radius: 10px;
          background: #b73636;
          color:white;
          margin: 0% 3%;
          margin-bottom:1%;
          padding: 1%;
          width: 50%;
          font-family: Cursive;
          height:80px;
        }
        .home{
          display:inline-block;
          width:20%;
          height:80px;
          margin-top:1%;
          background-color: #28a745;
          border-radius: 10px;
        }
        .boutonConnexion{
          heigth:100%;
          margin-top:1%;
          font-family: Cursive;
          text-align:center;
          color:white;
          font-size:1.3em;
        }
        .boutonConnexion:hover{
          text-decoration: underline overline;
        }
        p{
          margin-bottom:1%;
        }

      body{
        background-color:#525252;
      }
      img{
        width: 15em;
        height: 18em;
      }
      a{
        width:20%;
      }
      .conteneur{
        display:inline-block;
        margin-left: auto;
        margin-right: auto;
        width: 30%;
      }
      .carte{
        display:inline-block;
        border: solid gray 1px;
        border-radius: 10px;
        width:30%;
        background-color: white;
        overflow: hidden;
        text-align: center;
        margin:3%;
      }

      .recherche{
        position:relative;
        left:25%;
      }

      @media screen and (max-width: 1024px) {
        .carte{
          display:block;
          border: solid gray 1px;
          border-radius: 10px;
          width:70%;
          background-color: white;
          overflow: hidden;
          text-align: center;
          margin:3%;
        }
      }

END;
      return $html;
    }

    public function render()
    {
      $app=\Slim\Slim::getInstance();
      $css=$this->css();
      if(!isset($_SESSION))
        session_start();
      $bouton1="Connexion";
      $bouton2="Inscription";
      $urlbout1=$app->urlFor("Connexion");
      $urlbout2=$app->urlFor("Inscription");
      if(isset($_SESSION['user'])){
        $bouton1="$_SESSION[user]";
        $bouton2="Changer d'utilisateur";
        $urlbout1=$app->urlFor("Utilisateur");
        $urlbout2=$app->urlFor("Connexion");
      }

$html=<<<END
<!DOCTYPE html>
<head>
  <title>My Wishlist</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"></head>
  <style>
  $css
  </style>
  <header class="page-header">
  <div>
    <h1 class="titre">My Wishlist</h1>
    <div class="home">
      <a href="$urlbout1"><p class="boutonConnexion">$bouton1</p></a>
      <a href="$urlbout2"><p class="boutonConnexion">$bouton2</p></a></div>
    </div>
  </div>
   <div class="recherche">
   <div class="row">
    <div class="col-5">
    <input id="input" class="form-control" type="text" placeholder="Entrez la clé de la liste" name="token"/>
    </div>
  <div class="col-1">
    <input id="bout" class="btn btn-success" type="submit" value="Ok" onclick="location.href='./liste/partage/'+document.getElementById('input').value;"/>
  </div>
  </div>
  </div>
 </header>
  <body>
  <center>
  <a href="./liste/souhait">
  <div class="carte">
    <img src="images/liste.jpg">
    <p><strong>Liste</strong></p>
  </div>
  </a>
  <a href="./item/create">
  <div class="carte">
    <img src="images/cadeau.jpg">
    <p><strong>Item</strong></p>
  </div>
  </a>
  </center>
</body>
</html>
END;
        echo $html;
    }
}
