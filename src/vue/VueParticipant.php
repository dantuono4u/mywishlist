<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 04/12/2018
 * Time: 16:44
 */

namespace wishlist\vue;

use wishlist\modele\Item;
use wishlist\modele\Liste;

class VueParticipant
{

    private $tab;
    private $titre = "Liste";
    private $url;

    function __construct($array){
        $this->tab = $array;
        $this->url = $this->getUrl();
    }

    private function getUrl(){
      $app =\Slim\Slim::getInstance() ;
      return $app->request->getRootUri()."/";
    }

    private function expiration($date){
      $expiration=strtotime($date);
      //date non expiré
      if($expiration>strtotime(date('o-m-j')))
        return false;
      //date expiré
      else
        return true;
    }
    private function pageErreur()
    {
      $this->titre="Erreur";
      return "<center><p>Lien Erroné</p><center>";
    }

    private function afficherListeCree(){
      $liste=$this->tab;
      $this->titre = "Liste : $liste[no] -- $liste[titre]" ;
      $lienPartage="$liste[token]";
      $lienDedition="$liste[tokenEdit]";
      $str="";
      $str.= "Id de l'utilisateur : $liste[user] - description : $liste[description] - expire le : $liste[expiration] <br><br>";
      $str.="<p style='color:red;'>Attention gardez bien la clé de partage et d'édition !</p><br>lien de partage : $lienPartage ---- lien d'édition : $lienDedition";
      return "<center>$str</center>";
    }

    private function listeUser(){
      $liste=$this->tab;
      $this->titre=$_SESSION['user'];
      $str="<center>";
      $urlCreate=$this->url."liste/create";
      if(!empty($liste)){
        foreach ($this->tab as $liste){
          $couleur="";
          if($this->expiration($liste['expiration']))
            $couleur="green";
          $urlfinal = $this->url."liste/".$liste['no'];
          $str.= "<a href=$urlfinal><p id=$couleur><strong>$liste[no] : $liste[titre] - $liste[description] - $liste[expiration]</strong></p></a>";
          $items=Item::where("liste_id","=",$liste['no'])->get();
          foreach ($items as $item) {
              $urlItem=$this->url."item/".$item['id'];
              $str .= "<a href=$urlItem><h5>$item[nom]</h5></a>";
          }
        }
      }
      return $str."<a href='$urlCreate' id='bouton' role='button' aria-pressed='true' class='btn btn-outline-success btn-lg active'>Ajouter une liste</a></center>";

    }

    private function css(){
      $urlIndex = $this->getUrl();
      $urlFond = $urlIndex."images/fond.jpg";
      $urlHeader = $urlIndex."images/header.jpg";
        $html=<<<end
        body {
          background-color:#525252;
        }
        header{
            background: url($urlHeader);
            text-align:center;
            padding:2%;
          }
        .titre{
            display: inline-block;
            border:solid black 2px;
            border-radius: 10px;
            background: #b73636;
            color:white;
            margin: 0% 2%;
            padding: 1%;
            width: 50%;
            font-family: Cursive;
          }
          p{
            border:solid 1px gray;
            border-radius: 10px;
            background-color: rgba(0,0,0,.1);
            color: #1828ff;
          }
          .home{
            display:inline-block;
            right:auto;
            position: relative;
            left: 15%;
            background-color: rgba(255,255,255,1);
            border:solid 2px black;
            border-radius: 10px;
          }
          #bouton{
            margin:1%;
          }
          #EnLigne{
            display: inline-block;
          }
          #green{
            color:green;
          }
end;
return $html;
    }
    private function afficherListeDeListe()
    {
        $this->titre = "Listes";
        $url=$this->getUrl()."liste/";
        $str="<center>";
        $urlAll=$this->getUrl()."liste/all";
        $urlCreate=$this->getUrl()."liste/create";
        foreach ($this->tab as $liste){
          $couleur="";
          if($this->expiration($liste['expiration']))
            $couleur="green";
          $urlfinal = $url.$liste['no'];
          $str.= "<a href=$urlfinal><p id=$couleur><strong>$liste[no] : $liste[titre] - $liste[description] - $liste[expiration]</strong></p></a>";
        }
        return $str."<a href='$urlAll' role='button' aria-pressed='true' id='bouton' class='btn btn-outline-success btn-lg active'>Afficher les Items</a><a href='$urlCreate' id='bouton' role='button' aria-pressed='true' class='btn btn-outline-success btn-lg active'>Ajouter une liste</a></center>";
    }

    private function afficher1Liste(){
        $liste=$this->tab;
        $this->titre = "Liste : $liste[no] -- $liste[titre]" ;
        $expiration=date('j-m-o',strtotime($liste['expiration']));
        $str="";
        $str.= "Id utilisateur : $liste[user] - description : $liste[description] - expire le : $expiration <br>";
        $items=Item::where("liste_id","=",$liste['no'])->get();
        foreach ($items as $item) {
            $strItem="<div>";
            $strItem.= "<a href=$this->url/item/$item[id] style='text-decoration: none'>--></a> $item->nom - description : $item->descr - prix : $item->tarif € - participant : $item->participant - message de réservation : $item->message";
            if($item->url!==""){
              $strItem = "<a href=$item->url target='_blank'>$strItem</a>";
            }
            if($item['img']!==null){
              $urlImage=$this->getUrl()."images/imagesItem/$item->img";
              $strItem="<img src=$urlImage style='width:100px; heigth:100px;'></img>".$strItem;
            }
            $str.="$strItem<br>";
        }
        $str.=<<<END
        <from methode='post' action=>
END;

        /// ICI AJOUT DU MESSAGE A LA LISTE
        $app =\Slim\Slim::getInstance() ;
        $itemUrl = $app->urlFor('Message') ;
        $url =  $itemUrl ;
        $id=$liste['no'];
        $liste=Liste::find($id);
        $message="$liste->message <br>";
        $urlCopy= $this->getUrl()."liste/".$id ;

        $str .= <<<END
        <hr class="my-4">
        <p>Message de la liste : </p>$message
        <form method="post" action="$url">
        <hr class="my-4">
          <div class="form-group">
          <input type="hidden" name="no" value="$id">
          <input type="hidden" name="messagePrecedent" value="$message">
            <input class="form-control" placeholder="Message" id="exampleFormControlTextarea1" rows="2" name="message"></input>
          </div>
          <button class="btn btn-success" type="submit">Valider</button>
        </from>
END;
        $str ="<center> $str </center>";
        return $str;
    }

    private function formModifItem(){
      $item=$this->tab;
      $this->titre = $item['nom'];
      $image="<input type='file' name='Image' accept='image/png, image/jpeg'/>";
      if($item['img']!=null){
        $image="<input type='file' name='Image' value='$this->url/images/imagesItem/$item[img]' accept='image/png, image/jpeg'/>
        <img src=$this->url/images/imagesItem/$item[img] style='width:70px; heigth:70px;'>";
      }
      $str=<<<end
      <form method="post" action="$this->url/item/modifier/$item[id]" enctype="multipart/form-data">
        <h1 class="display-4">Modification d'un Item</h1>
        <div class="form-group">
          <div class="row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Nom" name="nom"  value="$item[nom]">
            </div>
            <div class="col">
                  $image
            </div>
          </div>
          <hr class="my-4">
          <div class="row">
            <div class="col-8">
              <input type="text" class="form-control" placeholder="Url" name="url" value="$item[url]">
            </div>
            <div class="col-4">
              <input type="text" class="form-control" placeholder="Tarif" name="tarif" value="$item[tarif]">
            </div>
          </div>
          <hr class="my-4">
          <div class="form-group">
            <label for="exampleFormControlTextarea1">Description :</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" name="descr" value="$item[descr]"></textarea>
          </div>
          <button class="btn btn-success" type="submit">valider</button>
        </div>
      </form>
      <form method="post" action="$this->url/item/modifier/$item[id]">
      </form>
end;
return $str;
    }
    private function afficher1Item(){
        session_start();
        $modifsup="";
        $item=$this->tab;
        $liste=Liste::find($item['liste_id']);
        $this->titre = $liste->titre;
        $img="";
        if($item["img"]!=null)
        $img="<img src='$this->url/images/imagesItem/$item[img]' style='width:80%;'><br>";
        $str= <<<END
        <center>
        <h1 style='display:inline-block;margin-right:20px;'>$item[nom]</h1><em style='display:inline-block;'>$item[tarif] €</em><br>
        $img
        <a href='$item[url]'>$item[url]</a>
        </center>
        Description :<br><center><p style="background-color:white;color:black">$item[descr] </p></center>
END;
        if($this->expiration($liste->expiration)){
          $str.="<br>-->participant : $item[participant] - message de réservation : $item[message]<br>";
        }
        if(isset($_SESSION['user'])){
          $strbout="";
          if($_SESSION['user']==$liste->user){
            $strbout.= <<<END
            <div class="btn-group" role="group" aria-label="">
            <form method="get" action="$this->url/item/modifier/$item[id]">
            <button class="btn btn-success" type="submit" style='border-top-right-radius:0px; border-bottom-right-radius:0px;'>Modifier</button>
            </form>
            <form method="post" action="$this->url/item/suppression/$item[id]">
            <button class="btn btn-success" type="submit" style='border-top-left-radius:0px; border-bottom-left-radius:0px;' >Supprimer</button>
            </form>
            </div>

END;
          }
          if($item["participant"]==""){
            $strbout= <<<END
            <form method="post" action="$this->url/item/reserver/$item[id]">
              <input type="hidden" name="participant" value="$_SESSION[user]">
              <input type="hidden" name="id" value="$item[id]">
              <div class="form-group">
              <input class="form-control" type="text" name="message" placeholder="Message">
              </div>
              <div class="btn-group" role="group" aria-label="">
              <button class="btn btn-success" type="submit">Réserver</button>
              </form>
              $strbout
              </div>
END;
          }
          $str.="<center>$strbout</center>";
}
        return $str;
    }

    private function ListeEtItem(){
        $this->titre = "Listes";
        $url=$this->getUrl()."liste/";
        $str="<center>";
        $urlListe=$this->getUrl()."liste/souhait";
        $urlCreate=$this->getUrl()."liste/create";
        foreach ($this->tab as $liste){
          $couleur="";
          if($this->expiration($liste['expiration'])){
            $couleur="green";
          }
          $urlfinal = $url.$liste['no'];
          $str.= "<a href=$urlfinal><p id=$couleur><strong>$liste[no] : $liste[titre] - $liste[description] - $liste[expiration] </strong></p></a>";
          $items=Item::where("liste_id","=",$liste['no'])->get();
          foreach ($items as $item) {
              $urlItem=$this->url."item/".$item['id'];
              $str .= "<a href=$urlItem><h5>$item[nom]</h5></a>";
          }
        }
        return $str."<a href='$urlListe' role='button' aria-pressed='true' id='bouton' class='btn btn-outline-success btn-lg active'>Ne pas afficher les Items</a><a href='$urlCreate' id='bouton' role='button' aria-pressed='true' class='btn btn-outline-success btn-lg active'>Ajouter une liste</a></center>";
    }

    private function formListe(){
        session_start();
        $app =\Slim\Slim::getInstance() ;
        //$rootUri = $app->request->getRootUri() ;
        $itemUrl = $app->urlFor('new_liste') ;
        $url =  $itemUrl ;
        $user="";
        if(isset($_SESSION['user']))
          $user=$_SESSION['user'];
        $str= <<<END
            <form method="post" action="$url">
              <h1 class="display-4">Création d'une liste</h1>
              <div class="form-group">
                <input class="form-control" type="text" placeholder="Titre de la liste" name="titre">
              </div>
              <div class="form-group">
                <div class="row">
                  <input type="hidden" class="form-control" name="user" value="$user">
                  <div class="col">
                    <input type="date" class="form-control" placeholder="Expiration" name="expiration">
                  </div>
                </div>
                <hr class="my-4">
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Description :</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" name="description"></textarea>
                </div>
                <button class="btn btn-success" type="submit">valider</button>
                <div class="form-check" id="EnLigne">
                  <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="check">
                  <label class="form-check-label" for="defaultCheck1">
                  Public
                  </label>
                </div>
              </div>
            </form>
END;
        return $str;
    }


    private function formItem(){
        $this->titre = "Item";
        $app =\Slim\Slim::getInstance() ;
        $itemUrl = $app->urlFor('new_item') ;
        $url =  $itemUrl ;
        $listepublic=Liste::where("public", "=", 1)->get();
        $option="";
        foreach ($listepublic as $list) {
          $idliste=$list['no'];
          $option.="<option value=$idliste>$list[titre]</option>";
        }
        session_start();
        if(isset($_SESSION['user'])){
          $listeUser=Liste::where("user","=",$_SESSION['user'])->get();
          foreach ($listeUser as $list) {
            $idliste=$list['no'];
            $option.="<option value=$idliste>$list[titre]</option>";
          }
        }

        $str= <<<END
            <form method="post" action="$url" enctype="multipart/form-data">
              <h1 class="display-4">Création d'un Item</h1>
              <div class="form-group">
                <div class="row">
                  <div class="col">
                    <input type="text" class="form-control" placeholder="Nom" name="nom" required>
                  </div>
                  <div class="col">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Id de la liste</label>
                    </div>
                    <select placeholder="Id de la liste" class="custom-select" id="inputGroupSelect01" name="liste_id" required>
                      $option
                    </select>
                    </div>
                  </div>
                  <div class="col">
                        <input type="file" name="Image" accept="image/png, image/jpeg"/>
                  </div>
                </div>
                <hr class="my-4">
                <div class="row">
                  <div class="col-8">
                    <input type="text" class="form-control" placeholder="Url" name="url">
                  </div>
                  <div class="col-4">
                    <input type="text" class="form-control" placeholder="Tarif" name="tarif">
                  </div>
                </div>
                <hr class="my-4">
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Description :</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" name="descr"></textarea>
                </div>
                <button class="btn btn-success" type="submit">Valider</button>
              </div>
            </form>

END;
        return $str;
    }

    private function formBook(){
        $item=$this->tab[0];
        $id=$item['id'];
        $this->titre = $item['nom'];
        $app =\Slim\Slim::getInstance() ;
        $itemUrl = $app->urlFor('reservation') ;
        $url =  $itemUrl ;

        $str=<<<END
<form method="post" action="$url">


<input type="hidden"
        name="id"
        value="$id"
        />

participant :
<input type="text"
        name="participant"
        />

message :
<input type="text"
        name="message"
        />

<button class="btn btn-success" type="submit">Valider</button>
</form>
END;
        return $str;
    }

    public function modifListe(){
        $liste=$this->tab[0];
        $app =\Slim\Slim::getInstance() ;
        $urlmodif =  $app->urlFor('modification');
        $urlsup =  $app->urlFor('suppression');
        $urlPartage = $_SERVER['HTTP_REFERER'];
        $urlPartage .="liste/partage/$liste[token]";
        $te=$liste['tokenEdit'];
        $this->titre=$liste['titre'];
        $description=$liste['description'];
        $exp=date('o-m-j',strtotime($liste['expiration']));
        if($liste['public']==1)
          $checked='checked';
        else {
          $checked='';
        }

        $str=<<<END
<form method="post" action="$urlmodif">
<input type="hidden" name="tokenEdit" value="$te" />
<div class="form-group">
<div class="row">
<div class="col">
  <label>Titre de la liste</label>
  <input class="form-control" type="text" name="titre" value="$this->titre"/>
  </div>
  <div class="col">
  <label>Date d'expiration :</label>
    <input type="date" class="form-control" name="expiration" value=$exp>
  </div>
  </div>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Description :</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" name="description"></textarea>
  </div>
  <button class="btn btn-success" type="submit">Valider</button>
<div class="form-check" id="EnLigne">
      <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="check" $checked />
      <label class="form-check-label" for="defaultCheck1">Public</label>
</div>
</form>
<hr class="my-4">
<form method="post" action="$this->url/item/create" enctype="multipart/form-data">
  Ajouter un item :
  <div class="form-group">
    <div class="row">
      <div class="col">
        <input type="text" class="form-control" placeholder="Nom" name="nom" required>
      </div>
      <input type=hidden name='liste_id' value='$liste[no]'>
      <div class="col-4">
        <input type="text" class="form-control" placeholder="Tarif" name="tarif">
      </div>
      <div class="col">
            <input type="file" name="Image" accept="image/png, image/jpeg"/>
      </div>
    </div>
  <hr class="my-2">
    <div class="form-group">
    <input type="text" class="form-control" placeholder="Url" name="url">
    <hr class="my-1">
      <textarea class="form-control" id="exampleFormControlTextarea1" rows="1" name="descr" placeholder="Description"></textarea>
    </div>
    <button class="btn btn-success" type="submit">Ajouter</button>
  </div>
</form>


<center>
<form method="post" action="$urlsup">
<input type="hidden"
        name="tokenEdit"
        value="$te"
        />
<button class="btn btn-success" type="submit">Supprimer la liste</button>
</from>
<button class="btn btn-success" type="button" onclick="partage('$urlPartage')">Partager</button>
</center>


END;
      return $str;
    }


    public function render($num)
    {
        switch ($num) {
            case -1 :
                {
                    $content = $this->pageErreur();
                    break;
                }
            case 1 :
                {
                    $content = $this->afficherListeDeListe();
                    break;
                }
            case 2 :
                {
                    $content = $this->formModifItem();
                    break;
                }
            case 3 :
                {
                    $content = $this->afficher1Item();
                    break;
                }
            case 4 :
                {
                    $content = $this->formListe();
                    break;
                }
            case 5 :
                {
                    $content = $this->formItem();
                    break;
                }
            case 6 :
                {
                    $content = $this->afficher1Liste();
                    break;
                }
            case 7 :
                {
                    $content = $this->ListeEtItem();
                    break;
                }
            case 8 :
                {
                    $content = $this->formBook();
                    break;
                }
            case 9 :
                {
                    $content = $this->modifListe();
                    break;
                }
            case 10:
                {
                    $content = $this->supprListe();
                    break;
                }
            case 12 :
                {
                    $content = $this->modifierItem();
                    break;
                }
            case 13 :
                {
                    $content = $this->afficherListeCree();
                    break;
                }
            case 14 :
            {
                    $content = $this->listeUser();
                    break;
            }
        }
        $urlIndex = $this->getUrl();
        $urlHome = $urlIndex."images/home.png";
        $css=$this->css();
        $html=<<<END
        <!DOCTYPE html>
        <html>
        <head>
        <script type="text/javascript">
        function partage(url){
          navigator.clipboard.writeText(url).then(function() {
          /* clipboard successfully set */
          alert("Le lien est dans votre presse papier");
          }, function() {
          /* clipboard write failed */
          alert("le lien est : "+url);
          });
        }
        </script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"></head>
        <style>
        $css
        </style>
        <header class="page-header">
         <h1 class="titre">$this->titre</h1>
         <a href="$urlIndex"><img src="$urlHome" class='home'></a>
        </header>
        <body>
        <div class="container" >
        <div class="jumbotron jumbotron-fluid">
        <div class="container">
        $content
        </div>
        </div>
        </div>
        </body>
        </html>
END;
          echo $html;
    }

}
