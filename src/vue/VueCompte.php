<?php

namespace wishlist\vue;

class VueCompte {

	private $titre="Connexion";
	private $couleur1, $couleur2;
  function __construct($couleur1="", $couleur2=""){
		$this->couleur1=$couleur1;
		$this->couleur2=$couleur2;
	}

	public function css()
	{
		$app =\Slim\Slim::getInstance() ;
		$urlIndex = $app->request->getRootUri()."/";
		$urlFond = $urlIndex."images/fond.jpg";
		$urlHeader = $urlIndex."images/header.jpg";
		$html=<<<END
		header{
				background: url($urlHeader);
				text-align:center;
				padding:2%;
			}
			.titre{
				display: inline-block;
				border:solid black 2px;
				border-radius: 10px;
				background: #b73636;
				color:white;
				margin: 0% 2%;
				padding: 1%;
				width: 50%;
				font-family: Cursive;
			}
		.connexion{
			position: relative;
			left: 50%;
		}
		body{
			background-color:#525252;
		}

		.menu{
			background-color:#adadad;
			margin:10%;
			margin-top:3%;
			margin-bottom:3%;
			padding:2%;
			border:solid black 3px;
			border-radius: 10px;
		}
		.form-control{
			margin:2%;
			width: 90%;
		}
		.home{
			display:inline-block;
			right:auto;
			position: relative;
			left: 15%;
			background-color: rgba(255,255,255,1);
			border:solid 2px black;
			border-radius: 10px;
		}

		#EnLigne{
			display: inline-block;
		}
		#connexion{
			position: relative;
			left: 82%;
			margin: 1%;
END;
return "<style>$html</style>";
}
	public function connexion($message1="",$message2=""){
		$app = \Slim\Slim::getInstance() ;
		$url = $app->request->getRootUri();
		$html = <<<END
		<form method="post" action="$url/Connexion">
		<div class="menu">
		<center>
		<div>
		<input class="form-control" type="text" placeholder="Identifiant" name="user" style="border-color: $this->couleur1;" required>
		<p style="color:red;">$message1<p>
		</div>
		<div>
		<input class="form-control" type="password" placeholder="Mot de passe" name="mdp" style="border-color: $this->couleur2;" required>
		<p style="color:red;">$message2<p>
		</div>
		</center>
		<button id="connexion" class="btn btn-success" type=submit name="">Connexion</button>
		</div>
		</form>
		<center>
		<a  class="btn btn-success" href="$url/Inscription" role="button">S'inscrire</a>
		</center>

END;

return $html;
}
	public function creation($message=""){
		$app = \Slim\Slim::getInstance() ;
		$url = $app->request->getRootUri();
		$html = <<<END
		<form method="post" action="">
		<div class="menu">
		<center>
		<input class="form-control" type="text" placeholder="Identifiant" name="user" style="border-color: $this->couleur1;" required>
		<p style="color:red;">$message<p>
		<input class="form-control" type="password" placeholder="Mot de passe" name="mdp" required>
		<input class="form-control" type="password" placeholder="Confirmer" name="C_mdp" required>
		</center>
		<button id="connexion" class="btn btn-success" type=submit name="">S'inscrire</button>
		</div>
		</form>
		<center>
		<a  class="btn btn-success" href="$url/Connexion" role="button">Se connecter</a>
		</center>
END;
	return $html;
	}

	public function render($i, $message1="",$message2=""){
		$app = \Slim\Slim::getInstance();
		$urlIndex = $app->request->getRootUri();
		$css = $this->css();
		$urlHome = $urlIndex."/images/home.png";
		switch($i){
			case 1 : {
				$content=$this->connexion($message1,$message2);
				break;
			}

			case 2 : {
				$content=$this->creation($message1);
				break;
			}
		}
		echo <<<END
		<!DOCTYPE html>
    <html>
    <head>
    <title>My Wishlist</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
$css
</head>
<header class="page-header">
 <h1 class="titre">$this->titre</h1>
 <a href="$urlIndex"><img src="$urlHome" class='home'></a>
</header>
		$content
    </body><html>
END;


	}
}
