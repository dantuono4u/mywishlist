<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 21/11/2018
 * Time: 09:00
 */

namespace wishlist\modele ;

class Liste extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'liste' ;
    protected $primaryKey = 'no' ;
    public $timestamps = false;

    public function items() {
        return $this->hasMany('\wishlist\modele\Item', 'liste_id');
    }
}
