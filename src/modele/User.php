<?php

namespace wishlist\modele;

class User extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'user';
    protected $primaryKey = 'user';
    public $timestamps = false;
}
