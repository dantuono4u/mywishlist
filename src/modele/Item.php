<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 21/11/2018
 * Time: 08:55
 */

namespace wishlist\modele ;

class Item extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'item' ;
    protected $primaryKey = 'id' ;
    public $timestamps = false ;

    public function liste() {
        return $this->belongsTo('\wishlist\modele\Liste', 'liste_id');
    }
}
